<?php
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class testModel extends DB{
  public function __construct() {
    parent::__construct();
  }
  public function get_users(){
    $result = $this->_db->query('SELECT * FROM usuarios');
    $users = $result->fetch_all(MYSQLI_ASSOC);
    return $users;
  }
}


$testModel = new testModel();
$a_users = $testModel->get_users();
foreach ($a_users as $row){
  echo $row['nombre'];
}

?>
