<?php
/**
* Permite realizar el manejo de errores
* y depuración de variables
*
* @author iosoft
* @author http://www.iosoft.in
*
*/
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class Error extends DB{
  private static $instance = null;

  /**
  * Envía el Exception final
  *
  * @param string error a mostrar
  */
  public function __construct($error){
    $mensaje = "[ioframe][".date('Y-m-d h:i:s')."]: ". $error;
    throw new Exception($mensaje);
  }

  /**
  * Depura y muestra el contenido de una variable
  *
  * @param mixed $var variable a depurar
  * @return string variable depurada
  */
  public static function debug($var){
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
  }

  /**
  * Guarda el error en un archivo de texto
  *
  * @param string $error texto del error que se guardará en el log de errores
  */
  public static function log($error){
    file_put_contents(ROOT.DS.'logfile.txt', $error . "\r\n", FILE_APPEND);
  }

  /**
  * Muestra el error
  *
  * @param string $error texto del error que se mostrará
  * @return object Exception del error
  */
  public static function mostrar($error) {
    if(self::$instance == null) {
        self::$instance = new Error($error);
    }
    return self::$instance;
  }
  private function __clone(){
  }
  private function __wakeup(){
  }
}
?>
