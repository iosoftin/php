<?php
/**
* Permite realizar el manejo de las rutas
*
* @author iosoft
* @author http://www.iosoft.in
*
*/
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class Route{
	private $_listUri = array();
	private $_listCall = array();
	private $_trim = '/\^$';

	/**
	* Agrega una funcion a la ruta especificada
	*
	* @param string $uri la ruta a llamar
	* @param object $function una función anonima
	*/
	public function add($uri, $function){
		$uri = trim($uri, $this->_trim);
		$this->_uri[] = $uri;
		$this->_func[] = $function;
	}

	public function submit(){
		$uri = isset($_REQUEST['uri']) ? $_REQUEST['uri'] : '/';
		$uri = trim($uri, $this->_trim);
		$parametros = array();
    $error = 0;
    $count = 0;
		foreach ($this->_uri as $key => $value){
      $count++;
			//if (preg_match("#^$value#", $uri)){
      if($value == $uri){
        if(is_callable($this->_func[$key])){
          $rxealUri = explode('/', $uri);
          $fakeUri = explode('/', $value);
          foreach ($fakeUri as $key => $value){
          	if ($value == '.+'){
          		$parametros[] = $realUri[$key];
          	}
          }
          call_user_func_array($this->_func[$key], $parametros);
        }else{
          $useMethod = $this->_func[$key];
          new $useMethod();
        }
			}else{
        $error++;
      }
		}
    if($error==$count){
      Error::mostrar("Error 404");
    }
	}
}

?>
