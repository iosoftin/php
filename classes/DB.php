<?php
/**
* Permite realizar varias acciones
* para el manejo de bases de datos
*
* @author iosoft
* @author http://www.iosoft.in
*
*/
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class DB{
  /**
  * Instancia de MySQLi
  * @var object $_db
  * @access public
  */
  public $_db;

  /**
  * Crea una instancia de MySQLi
  *
  * @return object Instancia de la base de datos
  */
	public function __construct(){
	  $this->_db = new mysqli(Config::get('host'), Config::get('user'), Config::get('pass'), Config::get('db'));
    if($this->_db->connect_errno){
      Error::mostrar('Error al conectar con MySQL');
    }
    return $this->_db;
	}

  /**
  * Construye la parte del WHERE.
  *
  * @param string $table Nombre de la tabla
  * @param mixed $where Int o Array del identificador para el WHERE [$key => $value]
  * @return string Clausula WHERE
  */
  private function _where($table, $where){
    $wh = null;
    if(is_numeric($where)){
      $wh = " WHERE {$table}_id = '{$where}'";
    }elseif (is_array($where)){
      $wh = '';
      foreach ($where as $key => $value) {
        $wh .= "{$table}_{$key} = '{$value}' AND ";
      }
      $wh = " WHERE " . rtrim($wh, ' AND ');
    }
    return $wh;
  }

  /**
  * Inserta una fila a la tabla especificada
  *
  * @param string $table Nombre de la tabla
  * @param array $columns Columnas a editar de la base de datos [$key => $value]
  * @param mixed $where Int o Array del identificador para el WHERE [$key => $value]
  * @return int Columnas editadas
  */
  public function insert($table, $data){
    $keys = '';
    $values = '';
    foreach ($data as $key => $value) {
      $keys .= "{$table}_{$key}, ";
      $keys .= "'{$value}', ";
    }
    $keys = rtrim($keys, ', ');
    $values = rtrim($values, ', ');

    $keys   = implode(", ", array_keys($data));
    $params = "'".implode("', '", array_values($data))."'";
    if($this->_db->query("INSERT INTO {$table} ({$keys}) VALUES ({$values})")){
      return $this->_db->insert_id;
    }else{
      Error::mostrar("Error al agregar un campo a la base de datos");
      return 0;
    }
  }

  /**
  * Selecciona una serie de filas de la tabla especificada
  *
  * @param string $table Nombre de la tabla
  * @param mixed $columns String o Array de las columnas de la base de datos
  * @param mixed $where Int o Array del identificador para el WHERE [$key => $value]
  * @return object objeto de mysql
  */
  public function select($table, $columns = '*', $where = null) {
    if(is_array($columns)) {
      $col = '';
      foreach ($columns as $column) {
        $col .= "{$table}_{$column}, ";
      }
      $columns = rtrim($col, ', ');
    }
    $where = $this->_where($table, $where);
    if($stmt = $this->_db->query("SELECT {$columns} FROM {$table}{$where}")){
      return $stmt;
    }else{
      Error::mostrar("Error al seleccionar un campo de la base de datos");
      return 0;
    }
  }

  /**
  * Edita una serie de filas de la tabla especificada
  *
  * @param string $table Nombre de la tabla
  * @param array $columns Columnas a editar de la base de datos [$key => $value]
  * @param mixed $where Int o Array del identificador para el WHERE [$key => $value]
  * @return int Columnas afectadas
  */
  public function update($table, $data, $where) {
    $set = '';
    foreach ($data as $key => $value) {
        $set .= "{$table}_{$key} = '{$value}',";
    }
    $set = rtrim($set, ',');
    $where = $this->_where($table, $where);
    if($this->_db->query("UPDATE {$table} SET {$set}{$where}")){
      return $sth->affected_rows;
    }else{
      Error::mostrar("Error al editar un campo en la base de datos");
      return 0;
    }
  }

  /**
  * 'Elimina' un elemento de la base de datos.
  *
  * @param string $table Nombre de la tabla
  * @param mixed $where Int o Array del identificador para el WHERE [$key => $value]
  * @return int Columnas afectadas
  */
  public function delete($table, $where) {
    $where = $this->_where($table, $where);
    if($where != NULL && $where != ''){
      if($this->_db->query("UPDATE {$table} SET {$table}_status = 0 {$where}")){
        return $sth->affected_rows;
      }else{
        Error::mostrar("Error al eliminar un campo en la base de datos");
        return 0;
      }
    }else{
      Error::mostrar("Error al eliminar un campo en la base de datos, no se especificó una condición");
    }
  }
}
?>
