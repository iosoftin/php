<?php
/**
* Clase Tokens para formularios
* aumento de seguridad en los links
*
* @author iosoft
* @author http://www.iosoft.in
*
*/
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class Tokens{
  /**
  * Función para obtener los caracteres aleatorios generados con numeros y letras
  */
  private function obtenCaracterAleatorio($arreglo) {
  		$clave_aleatoria = array_rand($arreglo, 1);
  		return $arreglo[ $clave_aleatoria ];
  }

  /**
  * Función para convertir a MD5 la cadena aleatoria obtenida
  */
  private function obtenCaracterMd5($car) {
		$md5Car = md5($car.Time());
		$arrCar = str_split(strtolower($md5Car));
		$carToken = $this->obtenCaracterAleatorio($arrCar);
		return $carToken;
	}

  /**
  * Función que genera la cadena aleatoria de textos y números
  */
  private function obtenToken($longitud) {
		//crear alfabeto
		$minus = "abcdefghijklmnopqrstuvwxyz";
		$minuscula = str_split($minus);
		$numeros = range(0,9);
		shuffle($minuscula);
		shuffle($numeros);
		$arregloTotal = array_merge($minuscula,$numeros);
		$newToken = "";

		for($i=0;$i<=$longitud;$i++) {
				$miCar = $this->obtenCaracterAleatorio($arregloTotal);
				$newToken .= $this->obtenCaracterMd5($miCar);
		}
		return $newToken;
	}

  /**
  * Se genera el token, se inserta en DB y se devuelve el dato
  */
  public function generar_Token(){
    $bdd = new DB;
    $exisToken = "";
    $tmpToken = $this->obtenToken(30);
    $nuevoToken = $this->obtenToken(30);
    $result = $bdd->select("tokens",array("token"), array("token" => $tmpToken));
    while($resultados = $asd->fetch_assoc()){
      $exisToken = $resultados['token'];
    }
    if ($exisToken != "" or $exisToken != NULL){
      $result = $bdd->insert('tokens', array("token" => $nuevoToken));
      print $nuevoToken;
    } else {
      $result = $bdd->insert('tokens', array("token" => $nuevoToken));
      print $nuevoToken;
    }
  }


  /**
  * Luego que el token fue utilizado se procede a retornar un OK y elimnarse
  */
  public function usar_token($token){
    $bdd = new DB;
    $exisToken = "";
    $result = $bdd->select("tokens",array("token"), array("token" => $token));
    while($resultados = $asd->fetch_assoc()){
      $exisToken = $resultados['token'];
    }
    if ($exisToken != "" or $exisToken != NULL){
      $result = $bdd->delete("tokens", array("token" => $token));
      print "ok";
    } else {
      print "token invalido";
    }
  }
}
?>
