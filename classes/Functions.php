<?php
class functions{
  public static function getFechaEnLetras($fecha = "", $boolFechaDeHoy = true, $boolShowDiaSemana = false){
    setlocale(LC_TIME, "Spanish");
    $fechaReturn = "";
    if($boolFechaDeHoy){
      $fechaReturn = date('l \t\h\e jS');
      $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
      $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
      $year = date('Y');
      $dia = date('d');
      $diaSemana = $dias[date('w')];

      if($boolShowDiaSemana){
        $diaSemana = $dias[date('w')];
      }
      $fechaReturn = "{$diaSemana} {$dia} de ".$meses[date('n')]." del {$year}";
    }
    else{
     /*
      	 if (DateTime::createFromFormat('d-m-y h:i:s', $fecha) !== FALSE) {
		 	echo 'true';
		} else {
			echo 'false';
			}
	 */
      $fecha = strtotime($fecha);
      $fechaReturn = strftime('%A %d de %B del %Y a las %H:%I:%S', $fecha);
    }
    print $fechaReturn;
  }

}
?>
