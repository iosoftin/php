<?php
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class Test{
  public function __construct(){
    //parent::__construct();
    $d = new DB;
    // INSERT
    //$asd = $d->insert('test', array("nombre" => "Luis", "apellido"=>"Leiva"));        INSERT INTO test (nombre, apellido) VALUES ('Luis', 'Leiva')

    // SELECT
    // $asd = $d->select('test');                                                       SELECT * FROM test
    // $asd = $d->select('test', "nombre");                                             SELECT nombre FROM test
    // $asd = $d->select('test', array("nombre", "apelido"));                           SELECT test_nombre, test_apellido FROM test
    // $asd = $d->select('test', array("nombre", "apelido"), 1);                        SELECT test_nombre, test_apellido FROM test WHERE test_id = 1
    // $asd = $d->select('test', array("nombre", "apelido"), array("nombre"=>"luis"));  SELECT test_nombre, test_apellido FROM test WHERE test_nombre = 'luis'
    // while($resultados = $asd->fetch_assoc()){
    //   echo $resultados['nombre'];
    // }

    // UPDATE
    // $asd = $d->update('test', ['nombre'=>'Luis'], 1);                               UPDATE test SET test_nombre = 'Luis' WHERE test_id = 1
    // $asd = $d->update('test', ['nombre'=>'Luis'], array('apellido'=>'Leiva'));       UPDATE test SET test_nombre = 'Luis' WHERE test_apellido = 'Leiva'

    // DELETE
    // $asd = $d->delete('test', 1));                                                   UPDATE test SET test_status = 0 WHERE test_id = 1
    // $asd = $d->delete('test', array('apellido'=>'Leiva'));                           UPDATE test SET test_status = 0 WHERE test_apellido = 'Leiva'

    // $d->query('SELECT a JOIN b WHERE')
  }
  public function _mundo(){
    echo "mundo!";
  }
}
?>
