<?php
/**
* Sistema de templates muy básico
*
* @author iosoft
* @author http://www.iosoft.in
*
*/
class TPL{
  var $vars;
  /**
   * Crea la instancia para el template
   *
   * @param $tpl template
   */
  function __construct($tpl = null) {
    $this->tpl = $tpl;
  }

  /**
   * Crea una variable para usarla en el template
   *
   * @param string $name nombre de la variable
   * @param mixed $value valor de la variable
   */
  function assign($name, $value) {
    if(is_object($value)){
      $this->vars[$name] = $value->fetch();
    }else{
      $this->vars[$name] = $value;
    }
  }

  /**
   * Parsea el template
   *
   * @param $tpl template
   * @return string contenido del template
   */
  function draw($tpl = null) {
    if(!$tpl){
      $tpl = $this->tpl;
    }
    extract($this->vars);
    ob_start();
    include($tpl);
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
  }
}
?>
