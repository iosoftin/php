<?php
/**
* Clase básica que permite añadir constantes
* globales al rededor de todo el código.
*
* @author iosoft
* @author http://www.iosoft.in
*
*/
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
class Config{
  /**
  * Crea una variable global.
  *
  * @param string $key key única para la constante
  * @param mixed $value valor de la constante previamente creada
  */
  public static function set($key, $value){
    $GLOBALS['conf'][$key] = $value;
  }

  /**
  * Obtiene el valor de una constante creada con el método Config::set.
  *
  * @param string $key key única para la constante
  * @return mixed valor de la constante $key
  */
  public static function get($key) {
    return $GLOBALS['conf'][$key];
  }
}
?>
