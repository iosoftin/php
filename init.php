<?php
define('DEBUG', TRUE);
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
$GLOBALS['conf'] = array();
if(!DEFINED('ACCESS')){
  exit("Error: Acceso restringido");
}
function auto_loader($classname){
  if(is_file("classes/".$classname.".php")){
    require_once "classes/".$classname.".php";
  }else{
    Error::mostrar("Clase {$classname} no encontrada");
  }
}
spl_autoload_register('auto_loader');
?>
